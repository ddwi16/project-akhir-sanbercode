<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/admin', function () {
    return view('auth.adminLogin');
});

Route::get('/', function () {
    return view('landing_page.landing');
})->name('landing');

// ADMIN
Route::post('/adminLogin', ('AdminAuthController@login'));
Route::resource('admin/mobil', 'PostDataMobilController');
Route::resource('admin/user', 'UserController');
Route::get('admin/transaksi', 'TransaksiController@dashboardAdmin');

// USER
Auth::routes();
Route::get('/homepage', 'HomeController@index')->name('home');
Route::get('/homepage/profile/{id}', 'ProfileController@index');
Route::post('/homepage/transaksi', 'TransaksiController@store');
Route::get('/homepage/dashboard', 'TransaksiController@dashboard');
Route::get('/bukti_pembayaran/{id}/{user}', 'TransaksiController@bukti');
Route::put('/bukti_pembayaran/{id}', 'TransaksiController@kirimBukti');
Route::get('/homepage/ulasan', 'ReviewController@create');
Route::post('/homepage/ulasan', 'ReviewController@store');
Route::get('/details', 'ReviewController@detail');

// DOMPDF
Route::get('/homepage/dompdf/{id}', 'TransaksiController@pdf');
