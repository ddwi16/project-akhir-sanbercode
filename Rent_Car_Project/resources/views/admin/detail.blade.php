@extends('layouts.admin')

@section('content')
<div class="container">
    <a href="/admin/mobil" class="btn btn-primary mb-2 mt-2" >Kembali</a>
    <ul class="list-group">
      <li class="list-group-item">{{ $name }} ({{ $detail->tahun_mobil }})</li>
      <li class="list-group-item">{{ $detail->nomor_mesin }}</li>
      <li class="list-group-item">{{ $detail->tipe_mobil }}</li>
      <li class="list-group-item">{{ $detail->plat_nomor }}</li>
      <li class="list-group-item">
        <img src="../../images/fotoMobil/{{ $fotoMobil }}" alt="" style="width: 9rem;">
      </li>
    </ul>

</div>
@endsection