@extends('layouts.admin')

@section('content')
    <h1>Form Tambah Data Mobil</h1> 
    <div class="container">
        <form action="/admin/mobil/{{ $seri->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card text-dark bg-light mb-3">
                <div class="card-body">
            <div class="form-group">
                <label for="seri">Seri Mobil</label>
                <input type="text" class="form-control" name="seri" id="seri" placeholder="{{ $seri->seri_mobil }}">
            </div>
            <div class="form-group">
                <label for="harga">Harga Mobil</label>
                <input type="text" class="form-control" name="harga" id="harga" placeholder="{{ $seri->harga_sewa }}">
            </div>
            <div class="form-group">
                <label for="nomorMesin">Nomor Mesin</label>
                <input type="text" class="form-control" name="nomorMesin" id="nomorMesin" placeholder="{{ $detail->nomor_mesin }}">
            </div>
            <div class="form-group">
                <label for="tipe">Tipe Mobil</label>
                <input type="text" class="form-control" name="tipe" id="tipe" placeholder="{{ $detail->tipe_mobil }}">
            </div>
            <div class="form-group">
                <label for="platNomor">Plat Nomor</label>
                <input type="text" class="form-control" name="platNomor" id="platNomor" placeholder="{{ $detail->tipe_mobil }}">
            </div>
            <div class="form-group">
                <label for="tahun">Tahun Mobil</label>
                <input type="text" class="form-control" name="tahun" id="tahun" placeholder="{{ $detail->tahun_mobil }}">
            </div>
            <div class="form-group">
                <img src="../../../images/fotoMobil/{{ $foto }}" class="card-img-top mb-2" style="width: 9rem;" alt="..."></br>
                <label for="fotoMobil">Foto Mobil</label></br>
                <input type="file" name="fotoMobil" id="fotoMobil">
            </div>
            <button type="submit" class="btn btn-primary mb-3">Tambah</button>
        </div>
        </div>
        </form>
    </div>  
@endsection