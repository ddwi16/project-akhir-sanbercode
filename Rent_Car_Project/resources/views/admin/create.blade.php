@extends('layouts.admin')

@section('content')
    <h1>Form Tambah Data Mobil</h1> 
    <div class="container">
        <form action="/admin/mobil" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="seri">Seri Mobil</label>
                <input type="text" class="form-control" name="seri" id="seri" placeholder="Masukkan Seri">
            </div>
            <div class="form-group">
                <label for="harga">Harga Mobil</label>
                <input type="text" class="form-control" name="harga" id="harga" placeholder="Masukkan Harga">
            </div>
            <div class="form-group">
                <label for="nomorMesin">Nomor Mesin</label>
                <input type="text" class="form-control" name="nomorMesin" id="nomorMesin" placeholder="Masukkan Nomor Mesin">
            </div>
            <div class="form-group">
                <label for="tipe">Tipe Mobil</label>
                <input type="text" class="form-control" name="tipe" id="tipe" placeholder="Masukkan Tipe Mobil">
            </div>
            <div class="form-group">
                <label for="platNomor">Plat Nomor</label>
                <input type="text" class="form-control" name="platNomor" id="platNomor" placeholder="Masukkan Plat Nomor">
            </div>
            <div class="form-group">
                <label for="tahun">Tahun Mobil</label>
                <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
            </div>
            <div class="form-group">
                <label for="fotoMobil">Foto Mobil</label><br>
                <input type="file" name="fotoMobil" id="fotoMobil">
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    
    </div>   
@endsection