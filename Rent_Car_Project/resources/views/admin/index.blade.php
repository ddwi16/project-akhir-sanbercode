@extends('layouts.admin')

@section('content')
<div class="container">
    <a href="/admin/mobil/create" class="btn btn-primary mb-2 mt-2" >Tambah Mobil</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col" class="col-md-4">no</th>
            <th scope="col" class="col-md-4">seri mobil</th>
            <th scope="col" class="col-md-4">aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          // dd($mobil);
            if(!isset($mobil)){
              $mobil = [];
            }
            ?>
          <?php foreach ($mobil as $m) : 
          // dd($m);
          ?>
          <tr class="">
            <td scope="rowgroup" class="col-md-3">{{ $m->id }}</td>
            <td scope="rowgroup" class="col-md-3">{{ $m->seri_mobil }}</td>
            <td scope="rowgroup" class="col-md-3">
              <a href="/admin/mobil/{{ $m->id }}" class="btn btn-warning">Details</a>
              <a href="/admin/mobil/{{ $m->id }}/edit" class="btn btn-primary" >Edit</a>
              <form action="/admin/mobil/{{$m->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Delete">
            </form></td>
              
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
</div>
<script>
  @include('sweetalert::alert')
</script>

@endsection