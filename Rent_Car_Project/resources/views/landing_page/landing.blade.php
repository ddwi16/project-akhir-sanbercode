<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Rent Car</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('carbook-master/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('carbook-master/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/jquery.timepicker.css')}}">
    
    <link rel="stylesheet" href="{{asset('carbook-master/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/style.css')}}">
    @yield('packages')
  </head>

  <body>
	@include('landing_page.partial.navbar')
    <!-- END nav -->
    
    @include('auth.login')
    @include('auth.register')

    <div class="hero-wrap ftco-degree-bg" style="background-image: url('carbook-master/images/car-3.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text justify-content-start align-items-center justify-content-center">
          <div class="col-lg-8 ftco-animate">
          	<div class="text w-100 text-center mb-md-5 pb-md-5">
	            <h1 class="mb-4">Fast &amp; Easy Way To Rent A Car</h1>
	            <p style="font-size: 18px;">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    @include('landing_page.partial.featured')

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  
  <script src="{{asset('carbook-master/js/jquery.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/popper.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/aos.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('carbook-master/js/jquery.timepicker.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/scrollax.min.js')}}"></script>
  <script src="{{asset('carbook-master/js/main.js')}}"></script>
    
  </body>
</html>