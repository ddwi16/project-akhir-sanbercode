<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Label - Premium Responsive Bootstrap 4 Admin & Dashboard Template</title>

    <link rel="stylesheet" href="{{asset('Label/src/assets/vendors/iconfonts/mdi/css/materialdesignicons.css')}}">
    <link rel="stylesheet" href="{{asset('Label/src/assets/css/shared/style.css')}}">
    <link rel="stylesheet" href="{{asset('Label/src/assets/css/demo_1/style.css')}}">
    <link rel="shortcut icon" href="{{asset('Label/src/asssets/images/favicon.ico')}}" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('carbook-master/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('carbook-master/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/jquery.timepicker.css')}}">

    <link rel="stylesheet" href="{{asset('carbook-master/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('carbook-master/css/style.css')}}">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
    @include('home.partial.carbook-style')
@include('home.partial.carbook-jquery')
  </head>
  <body class="header-fixed">
    <!-- partial:partials/_header.html -->
    @include('home.partial.header')
    {{-- modal --}}


    <!-- partial -->
    <div class="page-body">
      <!-- partial:partials/_sidebar.html -->
      @include('home.partial.sidebar')

      <!-- partial -->
      <div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
          <div class="content-viewport">
            <div class="row">
              <div class="col-12 py-5">
                <h4>@yield('page_title')</h4>
                <p class="text-gray">@yield('welcome_user')</p>
              </div>
            </div>
            <div class="row">
              @yield('content')
            </div>
          </div>
        </div>
        <!-- partial -->
      </div>
      <!-- page content ends -->
    </div>
    <script>
      Swal.fire(
        'Selamat Datang',
        'Anda berhasil masuk'
      )
    </script>
    
    <script src="{{asset('Label/src/assets/vendors/js/core.js')}}"></script>
    <script src="{{asset('Label/src/assets/vendors/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('Label/src/assets/vendors/chartjs/Chart.min.js')}}"></script>
    <script src="{{asset('Label/src/assets/js/charts/chartjs.addon.js')}}"></script>
    <script src="{{asset('Label/src/assets/js/template.js')}}"></script>
    <script src="{{asset('Label/src/assets/js/dashboard.js')}}"></script>

    <script src="{{asset('carbook-master/js/jquery.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/popper.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/aos.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('carbook-master/js/jquery.timepicker.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/scrollax.min.js')}}"></script>
    <script src="{{asset('carbook-master/js/main.js')}}"></script>
  </body>
</html>