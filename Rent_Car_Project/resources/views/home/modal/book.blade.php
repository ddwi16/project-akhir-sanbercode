<!-- Modal -->
<div class="modal fade p-0" id="bookingModal{{ $m->id }}{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="bookingModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="bookingModal">Form Booking</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php
        $id = $m->id;
        $data = DB::table('mobils')
            ->join('data_mobils', 'mobils.id', '=', 'data_mobils.id')
            ->select('data_mobils.*', 'mobils.*')->where('mobils.id',$id)
            ->get();
        $wrap = $data->all();
        $foto = $wrap[0]->foto_mobil;
        $pecah = explode("\\", $foto);
        $result = $pecah[3];
        // var_dump($wrap[0]->id);
        ?>
        <form method="POST" action="/homepage/transaksi">
            @csrf
        <div class="modal-body">
            <div class="col-auto">
                          <div class="car-wrap rounded ftco-animate">
                            <div class="img rounded d-flex align-items-end" style="background-image: url(images/fotoMobil/{{ $result }});">
                            </div>

                              <div class="text">
                                <h2 class="ml-auto d-flex justify-content-center">
                                    <a href="">{{ $wrap[0]->seri_mobil }}</a></h2>
                                <div class="mb-4 mt-0 d-flex justify-content-center">
                                    <span class="cat">Merek</span>
                                </div>
                                  <div class="col-auto">
                                      <input type="hidden" name="id_mobil" value="{{ $wrap[0]->id }}">
                                      <input type="hidden" name="id_user" value="{{ $user->id }}">
                                        <div class="form-group row">
                                            <label for="tgl_awal" class="col-md-3 col-form-label text-md-right">Awal Peminjaman</label>
                                            <div class="col-md-9">
                                                <input name="tgl_awal" id="tgl_awal" type="date" class="form-control"  placeholder="Date" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="tgl_akhir" class="col-md-3 col-form-label text-md-right">Akhir Peminjaman</label>
                                            <div class="col-md-9">
                                                <input name="tgl_akhir" id="tgl_akhir" type="date" class="form-control"  placeholder="Date" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="lama_peminjam" class="col-md-3 col-form-label text-md-right">lama peminjam</label>
                                            <div class="col-md-9">
                                                <input name="lama_peminjam"  id="lama_peminjam" class="form-control" placeholder="berapa hari meminjam" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="jenis_transaksi" class="col-md-3 col-form-label text-md-right">jenis transaksi</label>
                                            <div class="col-md-9">
                                                <input name="jenis_transaksi"
                                                id="jenis_transaksi" class="form-control" placeholder="jenis pembayaran" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="tujuan_peminjam" class="col-md-3 col-form-label text-md-right">tujuan peminjam</label>
                                            <div class="col-md-9">
                                                <input name="tujuan_peminjam" 
                                                id="tujuan_peminjam" class="form-control" placeholder="pernikahan, atau vacation." required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary">Transaksi Pembayaran</button>
                    </div>
        </form>
      </div>
    </div>
  </div> 
