<!-- plugins:css -->
<link rel="stylesheet" href="{{asset('Label/src/assets/vendors/iconfonts/mdi/css/materialdesignicons.css')}}">
<!-- endinject -->
<!-- vendor css for this page -->
<!-- End vendor css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{asset('Label/src/assets/css/shared/style.css')}}">
<!-- endinject -->
<!-- Layout style -->
<link rel="stylesheet" href="{{asset('Label/src/assets/css/demo_1/style.css')}}">
<!-- Layout style -->
<link rel="shortcut icon" href="{{asset('Label/src/asssets/images/favicon.ico')}}" />