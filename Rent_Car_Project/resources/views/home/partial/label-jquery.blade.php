<!-- SCRIPT LOADING START FORM HERE /////////////-->
    <!-- plugins:js -->
    <script src="{{asset('Label/src/assets/vendors/js/core.js')}}"></script>
    <!-- endinject -->
    <!-- Vendor Js For This Page Ends-->
    <script src="{{asset('Label/src/assets/vendors/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('Label/src/assets/vendors/chartjs/Chart.min.js')}}"></script>
    <script src="{{asset('Label/src/assets/js/charts/chartjs.addon.js')}}"></script>
    <!-- Vendor Js For This Page Ends-->
    <!-- build:js -->
    <script src="{{asset('Label/src/assets/js/template.js')}}"></script>
    <script src="{{asset('Label/src/assets/js/dashboard.js')}}"></script>
    <!-- endbuild -->