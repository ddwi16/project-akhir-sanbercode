<div class="sidebar">
    <div class="user-profile">
        <div class="display-avatar animated-avatar">
            <img class="profile-img img-lg rounded-circle" src="{{asset('Label/src/assets/images/profile/male/image_1.png')}}" alt="profile image">
        </div>
        <div class="info-wrapper">
            <p class="user-name">{{ Auth::user()->name }}</p>
            {{-- <h6 class="display-income">$3,400,00</h6> --}}
        </div>                 
    </div>
    
    <ul class="navigation-menu">
        <li>
            <a href="/homepage/dashboard">
            <span class="link-title">Dashboard</span>
            <i class="mdi mdi-credit-card-multiple link-icon"></i>
            </a>
        </li>
        <li>
            <a href="/homepage/profile/{{ Auth::user()->id }}">
            <span class="link-title">Profile</span>
            <i class="mdi mdi-account link-icon"></i>
            </a>
        </li>
        <li>
            <a href="/homepage">
            <span class="link-title">rent Car</span>
            <i class="mdi mdi-car link-icon"></i>
            </a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Logout') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </li>
    </ul>
</div>
