
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('carbook/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('carbook/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('carbook/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('carbook/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('carbook/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('carbook/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('carbook/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('carbook/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('carbook/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('carbook/css/style.css')}}">