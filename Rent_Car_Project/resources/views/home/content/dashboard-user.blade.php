@extends('home.home')
<?php
use App\mobil;
use App\data_mobil;
?>
@section('content')
<div class="page-content-wrapper">
    <div class="page-content-wrapper-inner">
      <div class="content-viewport">
        <div class="row">
                <div class="container">
                  <div class="col-md-9 ftco-animate pb-5">
                  <h1 class="mb-3 bread text-dark">Riwayat Penyewaan</h1>
                </div>
                </div>
          </div>
        </div>
        <?php foreach($dataPesan as $key) : ?>
        <?php
        // dd($key->mobil_id);
        $id_mobil = $key->mobil_id;
        $mobils = data_mobil::find($id_mobil);
        $mobil = mobil::find($id_mobil);
        $str = explode("\\" ,$mobils->foto_mobil);
        // dd($str);

        ?>
          <div class="card" style="height: 18em">
          <div class="container">
              <div class="row" scope="col">
                <div class="card col-ms-4 m-3 m-4" style="width: 14rem">
                  <img class="card-img-top" src="../../images/fotoMobil/{{ $str[3] }}" alt="Card image cap">
                  <div class="card-body text-center">
                    <div class="card-title">
                    <h4>{{ $mobil->seri_mobil }}</h4>
                  </div>
                  <div class="card-text">
                    {{ $mobils->tipe_mobil }}
                  </div>
                </div>
                </div>
                <div class="col-md-4 m-3 m-4">
                  <div class="my-5">
                    ({{ $key->mulai_sewa }}) - ({{ $key->akhir_sewa }})
                  </div>
                </div>
                <div class="col-md-4 m-3 m-4">
                  <div class="my-5 d-flex justify-content-end">
                    <a href="/homepage/dompdf/{{ $id_mobil }}" class="btn btn-info mr-3">Invoice</a>
                    <a href="/homepage/ulasan"><button type="button" class="btn btn-success">Berikan ulasan</button></a>
                  </div>
                </div>
                </div>
              </div>  
          </div>
        <?php endforeach; ?>
        </div>
        <footer class="footer">
          <div class="row">
            <div class="col-sm-6 text-center text-sm-right order-sm-1">
              <ul class="text-gray">
                <li><a href="#">Terms of use</a></li>
                <li><a href="#">Privacy Policy</a></li>
              </ul>
            </div>
            <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0">
              <small class="text-muted d-block">Copyright © 2019 <a href="http://www.uxcandy.co" target="_blank">UXCANDY</a>. All rights reserved</small>
              <small class="text-gray mt-2">Handcrafted With <i class="mdi mdi-heart text-danger"></i></small>
            </div>
          </div>
        </footer>
      </div>
      
    </div>
    <!-- content viewport ends -->
    <!-- partial:partials/_footer.html -->
    
    <!-- partial -->
@endsection