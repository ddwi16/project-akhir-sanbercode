@extends('home.home')

@section('content')
<?php
$user = Auth::user();
// dd($user->id);
?>
    <section class="ftco-section bg-light w-100">
    <div class="container">
        <div class="row">
            <?php foreach ($mobils as $m )  : ?>
           
            <?php
            $str = $m->foto_mobil;
          $pecah = explode("\\", $str);
          $result = $pecah[3];
          // dd($result);
            ?>
            @include('home.modal.book')
            <div class="col-md-4">
                <div class="car-wrap rounded ftco-animate">
                  <div class="img rounded d-flex align-items-end" style="background-image: url(images/fotoMobil/{{$result }});"></div>
                    <div class="text">
                        <h2 class="mb-0"><a href="/detail">{{ $m->seri_mobil }}</a></h2>
                        <div class="d-flex mb-3">
                            <span class="cat">{{ $m->tipe_mobil }}</span>
                            <p class="price ml-auto">${{ $m->harga_sewa }} <span>/day</span></p>
                        </div>
                        <p class="d-flex mb-0 d-block"><button type="button" class="btn btn-primary py-2 mr-1" data-toggle="modal" data-target="#bookingModal{{ $m->id }}{{ $user->id }}">Book now</button> <a href="/details" class="btn btn-secondary py-2 ml-1">Details</a></p>
                    </div>
                </div>
            </div>
            
            <?php endforeach; ?>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="row">
      <div class="col-sm-6 text-center text-sm-right order-sm-1">
        <ul class="text-gray">
          <li><a href="#">Terms of use</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0">
        <small class="text-muted d-block">Copyright © 2019 <a href="http://www.uxcandy.co" target="_blank">UXCANDY</a>. All rights reserved</small>
        <small class="text-gray mt-2">Handcrafted With <i class="mdi mdi-heart text-danger"></i></small>
      </div>
    </div>
  </footer>
@endsection