@extends('home.home')

@section('content')
<div class="container">
    <h1>Buat Profile</h1>
    <form action="/homepage/ulasan" method="POST">
        @csrf

        <div class="card text-dark bg-light mb-3">
            <div class="card-body">

        <div class="form-group">
            <label for="ulasan">Komentar</label></br>
            <textarea name="ulasan" id="ulasan" cols="123" rows="10"></textarea>
        </div>
        <div class="form-group">
            <label for="rate">Rating</label>
            <input type="text" class="form-control" name="rate" id="rate" placeholder="isi 1 - 10">
        </div>

        <button type="submit" class="btn btn-primary mb-3">kirim ulasan</button>
    </div>
    </div>
    </form>
</div>
@endsection