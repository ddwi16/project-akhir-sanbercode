@extends('home.home')

@section('content')
<?php
$user = Auth::user();
// dd($invoice);
// dd($user);
$book = $pesanan;
$str = $detail->foto_mobil;
$pecah = explode("\\", $str);
// dd($book->id);
?>
<div class="page-content-wrapper">
  <div class="page-content-wrapper-inner">
    <div class="content-viewport">
      <div class="row">
          <div class="col-12 py-5">
            <h4>Dashboard</h4>
            <p class="text-gray">Welcome aboard, Allen Clerk</p>
          </div>
        </div>
      <div class="row">
              <div class="container">
                <div class="col-md-9 ftco-animate pb-5">
                <h1 class="mb-3 bread text-dark">Riwayat Penyewaan</h1>
              </div>
              </div>
        </div>
      </div>

        <div class="card col-sm-12 d-flex justify-content-center mx-auto" style="width: 50%">
        <div class="container">
            <div class="row" scope="col">
                <div class="col-sm-12 d-flex justify-content-center">
                  <div class="card mb-3 mt-5" style="width: 10em, height:3em">
                      <div class="card-body">
                    <p class="m-4 text-center">invoice</p>
                    <p class="m-4 text-center">{{ $book->invoice }}</p>
                  </div>
                  </div>
                </div>
                <div class="col-md-12 d-flex justify-content-center">
                  <div class="card my-3" style="width: 15em">
                      <div class="card-body">
                  <div class="card-title text-center m-4"><p>Metode Pembayaran</p></div>
                    <img class="card-img-top"src="https://www.rajabeli.com/wp-content/uploads/2020/07/applikasi-gopay.png" alt="">
                    <p class="m-4 text-center">{{ $book->invoice }}</p>
                  </div>
                  </div>
                </div>
                <div class="col-md-12 d-flex justify-content-center">
                  <div class="card my-3" style="width: 15em">
                      <div class="card-body">
                    <img class="card-img-top"src="../../images/fotoMobil/{{ $pecah[3] }}" alt="">
                  </div>
                  </div>
                </div>
                <div class="col-md-12 d-flex justify-content-center my-4">
                  <ul class="list-group list-group-flush">
                      <li class="list-group-item">{{ $user->name }}</li>
                      <li class="list-group-item">{{ $user->email }}</li>
                      <li class="list-group-item">{{ $book->jumlah_hari }}</li>
                      <li class="list-group-item">{{ $book->tujuan }}</li>
                      <li class="list-group-item">{{ $data_mobil->seri_mobil }}</li>
                      <li class="list-group-item">$ {{ $data_mobil->harga_sewa }}</li>
                      <li class="list-group-item">{{ $detail->plat_nomor }}</li>
                    </ul>
                </div>
                <div class="col-md-12 d-flex justify-content-center mb-5">
                  <form action="/bukti_pembayaran/{{ $book->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="file" name="bukti" id="bukti"> 
                    <button type="submit" class="btn btn-outline-dark mr-5 mt-2">Kirim Bukti transfer</button>
                  </form>
                                      <!-- UPLOAD FILE BUKTI TRANSFER-->
                </div>
              </div>
            </div>
            
        </div>
      </div>
      <footer class="footer">
        <div class="row">
          <div class="col-sm-6 text-center text-sm-right order-sm-1">
            <ul class="text-gray">
              <li><a href="#">Terms of use</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>
          </div>
          <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0">
            <small class="text-muted d-block">Copyright © 2019 <a href="http://www.uxcandy.co" target="_blank">UXCANDY</a>. All rights reserved</small>
            <small class="text-gray mt-2">Handcrafted With <i class="mdi mdi-heart text-danger"></i></small>
          </div>
        </div>
      </footer>
    </div>
    
  </div>
  <!-- content viewport ends -->
  <!-- partial:partials/_footer.html -->
  
  <!-- partial -->
</div>
@endsection