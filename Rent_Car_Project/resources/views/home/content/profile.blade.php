@extends('home.home')

@section('content')
<div class="container">
    <form action="/homepage/user/{{ $data->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="card text-dark bg-light mb-3">
            <div class="card-body">
        <div class="form-group">
            <label for="name">Username</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="{{ $data->name }}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" id="email" placeholder="{{ $data->email }}">
        </div>
        <button type="submit" class="btn btn-primary mb-3">update</button>
    </div>
    </div>
    </form>
</div>  
<div class="container">
    <h1>Buat Profile</h1>
    <form action="/homepage/createProfile/{{ $data->id }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="card text-dark bg-light mb-3">
            <div class="card-body">

        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" placeholder="{{ $data->name }}">
        </div>
        <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap</label>
            <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="{{ $data->email }}">
        </div>
        <div class="form-group">
            <label for="no_ktp">No KTP</label>
            <input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="{{ $data->email }}">
        </div>
        <div class="form-group">
            <label for="foto_ktp">Foto KTP</label>
            <input type="text" class="form-control" name="foto_ktp" id="foto_ktp" placeholder="{{ $data->email }}">
        </div>
        <div class="form-group">
            <label for="gender">Gender</label>
            <input type="text" class="form-control" name="gender" id="gender" placeholder="{{ $data->email }}">
        </div>

        <button type="submit" class="btn btn-primary mb-3">update</button>
    </div>
    </div>
    </form>
</div>  
@endsection