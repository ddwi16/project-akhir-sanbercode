@extends('layouts.admin')

@section('content')
<table class="table">
    <thead>
      <tr>
        <th scope="col">mobil id</th>
        <th scope="col">user id</th>
        <th scope="col">tgl awal</th>
        <th scope="col">tgl akhir</th>
        <th scope="col">lama peminjaman</th>
        <th scope="col">invoice</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($dataPesan as $key) : ?>
        <tr>
        <td>{{ $key->mobil_id }}</td>
        <td>{{ $key->user_id }}</td>
        <td>{{ $key->mulai_sewa }}</td>
        <td>{{ $key->akhir_sewa }}</td>
        <td>{{ $key->jumlah_hari }}</td>
        <td>{{ $key->invoice }}</td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  
@endsection