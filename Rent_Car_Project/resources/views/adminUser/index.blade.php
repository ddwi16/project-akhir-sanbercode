@extends('layouts.admin')

@section('content')
<table class="table">
    <thead>
      <tr>
        <th scope="col">no</th>
        <th scope="col">username</th>
        <th scope="col">email</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $key) : ?>
      <tr>
        <th scope="row">{{ $key->id }}</th>
        <td>{{ $key->name }}</td>
        <td>{{ $key->email }}</td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
@endsection