@extends('home.home')

@section('content')
<?php
$user = Auth::user();
// dd($user->id);
?><!-- section -->
        @section('tipe-mobil')
            LCGC
        @endsection
        @section('nama-mobil')
            Brio
        @endsection
        @section('kilometer')
            25012
        @endsection
        @section('transmisi')
            Manual
        @endsection
        @section('seater')
            5 Seater
        @endsection
        @section('bagasi')
            258 Liter
        @endsection
        @section('jenis-bbm')
            Pertalite
        @endsection
        @section('deskripsi-parag1')
        New Honda Brio kini hadir dengan lebih banyak pilihan yang meliputi New Brio A/T 1.2 L yang menawarkan fitur lengkap dengan harga terjangkau, Brio Sports 1.3 L yang semakin sporty, serta Brio Satya yang merupakan produk Low Cost Green Car (LCGC) berkualitas tinggi dari Honda.
        @endsection
        @section('deskripsi-parag2')
        Untuk kenyamanan berkendara, terdapat pula fitur Tilt Steering yang memudahkan pengemudi untuk mengatur posisi tinggi rendahnya roda kemudi untuk mendapatkan posisi berkendara yang paling nyaman. Sementara, Electric Power Steering (EPS) berfungsi untuk membuat setir lebih ringan pada kecepatan rendah dan mantap pada kecepatan tinggi, sehingga manuver semakin lincah namun tetap stabil di berbagai kondisi jalan.
        @endsection
    <!-- end section -->
    <section class="ftco-section bg-light w-100">
    <div class="container">
        <div class="row">
            <?php //foreach ($mobils as $m )  : ?>
           
            <?php
            //$str = $m->foto_mobil;
          //$pecah = explode("\\", $str);
          //$result = $pecah[3];
          // dd($result);
            ?>
            <div class="col-xl-12">
                <section class="ftco-section ftco-car-details">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <div class="car-details">
                                    <div class="img rounded" style="background-image: url('https://www.honda-indonesia.com/uploads/images/models/sliders/banner_01__1613319275158.png');"></div>
                                    <div class="text text-center">
                                        <span class="subheading">@yield('tipe-mobil')</span>
                                        <h2>@yield('nama-mobil')</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md d-flex align-self-stretch ftco-animate">
                          <div class="media block-6 services">
                            <div class="media-body py-md-4">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-dashboard"></span></div>
                                    <div class="text">
                                      <h3 class="heading mb-0 pl-3">
                                          Mileage
                                          <span>@yield('kilometer')</span>
                                      </h3>
                                  </div>
                              </div>
                            </div>
                          </div>      
                        </div>
                        <div class="col-md d-flex align-self-stretch ftco-animate">
                          <div class="media block-6 services">
                            <div class="media-body py-md-4">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-pistons"></span></div>
                                    <div class="text">
                                      <h3 class="heading mb-0 pl-3">
                                          Transmission
                                          <span>@yield('transmisi')</span>
                                      </h3>
                                  </div>
                              </div>
                            </div>
                          </div>      
                        </div>
                        <div class="col-md d-flex align-self-stretch ftco-animate">
                          <div class="media block-6 services">
                            <div class="media-body py-md-4">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-car-seat"></span></div>
                                    <div class="text">
                                      <h3 class="heading mb-0 pl-3">
                                          Seats
                                          <span>@yield('seater')</span>
                                      </h3>
                                  </div>
                              </div>
                            </div>
                          </div>      
                        </div>
                        <div class="col-md d-flex align-self-stretch ftco-animate">
                          <div class="media block-6 services">
                            <div class="media-body py-md-4">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-backpack"></span></div>
                                    <div class="text">
                                      <h3 class="heading mb-0 pl-3">
                                          Luggage
                                          <span>@yield('bagasi')</span>
                                      </h3>
                                  </div>
                              </div>
                            </div>
                          </div>      
                        </div>
                        <div class="col-md d-flex align-self-stretch ftco-animate">
                          <div class="media block-6 services">
                            <div class="media-body py-md-4">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-diesel"></span></div>
                                    <div class="text">
                                      <h3 class="heading mb-0 pl-3">
                                          Fuel
                                          <span>@yield('jenis-bbm')</span>
                                      </h3>
                                  </div>
                              </div>
                            </div>
                          </div>      
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 pills">
                                      <div class="bd-example bd-example-tabs">
                                          <div class="d-flex justify-content-center">
                                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
              
                                              <li class="nav-item">
                                                <a class="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-expanded="true">Features</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" id="pills-manufacturer-tab" data-toggle="pill" href="#pills-manufacturer" role="tab" aria-controls="pills-manufacturer" aria-expanded="true">Description</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-expanded="true">Review</a>
                                              </li>
                                            </ul>
                                          </div>
              
                                        <div class="tab-content" id="pills-tabContent">
                                          <div class="tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
                                              <div class="row">
                                                  <div class="col-md-4">
                                                      <ul class="features">
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Airconditions</li>
                                                          <li class="remove"><span class="ion-ios-close"></span>Child Seat</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>GPS</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Luggage</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Music</li>
                                                      </ul>
                                                  </div>
                                                  <div class="col-md-4">
                                                      <ul class="features">
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Seat Belt</li>
                                                          <li class="remove"><span class="ion-ios-close"></span>Sleeping Bed</li>
                                                          <li class="check"><span class="ion-ios-close"></span>Water</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Bluetooth</li>
                                                          <li class="remove"><span class="ion-ios-close"></span>Onboard computer</li>
                                                      </ul>
                                                  </div>
                                                  <div class="col-md-4">
                                                      <ul class="features">
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Audio input</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Long Term Trips</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Car Kit</li>
                                                          <li class="check"><span class="ion-ios-checkmark"></span>Remote central locking</li>
                                                          <li class="remove"><span class="ion-ios-close"></span>Climate control</li>
                                                      </ul>
                                                  </div>
                                              </div>
                                          </div>
              
                                          <div class="tab-pane fade" id="pills-manufacturer" role="tabpanel" aria-labelledby="pills-manufacturer-tab">
                                            <p>@yield('deskripsi-parag1')</p>
                                                  <p>@yield('deskripsi-parag2')</p>
                                          </div>
              
                                          <div class="tab-pane fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                                            <div class="row">
                                                     <div class="col-md-7">
                                                         <h3 class="head">23 Reviews</h3>
                                                         <div class="review d-flex">
                                                             <div class="user-img" style="background-image: url(images/person_1.jpg)"></div>
                                                             <div class="desc">
                                                                 <h4>
                                                                     <span class="text-left">Jacob Webb</span>
                                                                     <span class="text-right">14 March 2018</span>
                                                                 </h4>
                                                                 <p class="star">
                                                                     <span>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                     </span>
                                                                     <span class="text-right"><a href="#" class="reply"><i class="icon-reply"></i></a></span>
                                                                 </p>
                                                                 <p>{{ $review->komentar }}</p>
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-5">
                                                         <div class="rating-wrap">
                                                             <h3 class="head">Give a Review</h3>
                                                             <div class="wrap">
                                                                 <p class="star">
                                                                     <span>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         (98%)
                                                                     </span>
                                                                     <span>20 Reviews</span>
                                                                 </p>
                                                                 <p class="star">
                                                                     <span>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         (85%)
                                                                     </span>
                                                                     <span>10 Reviews</span>
                                                                 </p>
                                                                 <p class="star">
                                                                     <span>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         (70%)
                                                                     </span>
                                                                     <span>5 Reviews</span>
                                                                 </p>
                                                                 <p class="star">
                                                                     <span>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         (10%)
                                                                     </span>
                                                                     <span>0 Reviews</span>
                                                                 </p>
                                                                 <p class="star">
                                                                     <span>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         <i class="ion-ios-star"></i>
                                                                         (0%)
                                                                     </span>
                                                                     <span>0 Reviews</span>
                                                                 </p>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                          </div>
                                        </div>
                                      </div>
                            </div>
                              </div>
                    </div>
                  </section>
              
                  <section class="ftco-section ftco-no-pt">
                      <div class="container">
                          <div class="row justify-content-center">
                        <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                            <span class="subheading">Choose Car</span>
                          <h2 class="mb-2">Related Cars</h2>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="car-wrap rounded ftco-animate">
                                  <div class="img rounded d-flex align-items-end" style="background-image: url('https://www.toyota.astra.co.id/sites/default/files/2020-07/2_Silver%20Metallic_0.png');">
                                  </div>
                                  <div class="text">
                                      <h2 class="mb-0"><a href="/details-calya">Calya</a></h2>
                                      <div class="d-flex mb-3">
                                          <span class="cat">MVP</span>
                                          <p class="price ml-auto">$500 <span>/day</span></p>
                                      </div>
                                      <p class="d-flex mb-0 d-block"><a href="#" class="btn btn-primary py-2 mr-1">Book now</a> <a href="/details-calya" class="btn btn-secondary py-2 ml-1">Details</a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="car-wrap rounded ftco-animate">
                                  <div class="img rounded d-flex align-items-end" style="background-image: url('https://assets.kompasiana.com/items/album/2017/07/27/kijang-inova-5979aa3a0a628c05f0064e62.jpg?t=o&v=770');">
                                  </div>
                                  <div class="text">
                                      <h2 class="mb-0"><a href="/details-innova">Innova</a></h2>
                                      <div class="d-flex mb-3">
                                          <span class="cat">Toyota</span>
                                          <p class="price ml-auto">$500 <span>/day</span></p>
                                      </div>
                                      <p class="d-flex mb-0 d-block"><a href="#" class="btn btn-primary py-2 mr-1">Book now</a> <a href="/details-innova" class="btn btn-secondary py-2 ml-1">Details</a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="car-wrap rounded ftco-animate">
                                  <div class="img rounded d-flex align-items-end" style="background-image: url('https://honda-kl.com/images/car-models/odyssey/colors/honda-odyssey-platinum-white-pearl.png');">
                                  </div>
                                  <div class="text">
                                      <h2 class="mb-0"><a href="car-single.html">Odyssey</a></h2>
                                      <div class="d-flex mb-3">
                                          <span class="cat">Honda</span>
                                          <p class="price ml-auto">$500 <span>/day</span></p>
                                      </div>
                                      <p class="d-flex mb-0 d-block"><a href="#" class="btn btn-primary py-2 mr-1">Book now</a> <a href="/details-odyssey" class="btn btn-secondary py-2 ml-1">Details</a></p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      </div>
                  </section>
            </div>
            
            <?php //endforeach; ?>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="row">
      <div class="col-sm-6 text-center text-sm-right order-sm-1">
        <ul class="text-gray">
          <li><a href="#">Terms of use</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0">
        <small class="text-muted d-block">Copyright © 2019 <a href="http://www.uxcandy.co" target="_blank">UXCANDY</a>. All rights reserved</small>
        <small class="text-gray mt-2">Handcrafted With <i class="mdi mdi-heart text-danger"></i></small>
      </div>
    </div>
  </footer>
@endsection