<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mobil extends Model
{
    protected $fillable = ['seri_mobil','harga_sewa'];

    public function data_mobil(){
        return $this->hasOne('App\data_mobil');
      }
}
