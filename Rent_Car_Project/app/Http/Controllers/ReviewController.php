<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function create()
    {
        return view('home.content.ulasan');
    }
    public function store(Request $request)
    {
        $user = Auth::user()->id;
        review::create([
            'user_id' => $user,
            'komentar' => $request->ulasan,
            'rating' => $request->rate
        ]);

        return redirect('/homepage');
    }
    public function detail()
    {
        $user = Auth::user()->id;
        $komen = review::all()->where('user_id', $user);
        $str = $komen->all();
        return view('layouts.details',[
            'review' => $str[0]
        ]);
    }
}
