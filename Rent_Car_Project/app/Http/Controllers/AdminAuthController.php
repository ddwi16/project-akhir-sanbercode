<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminAuthController extends Controller
{
    public function login(Request $request)
    {
        // dd($request->password);
        $email = $request->email;
        $password = $request->password;
        if ($email == 'admin@mail.com' && $password == 'admin123') {
            return redirect('/admin/mobil');
        } else {
            return redirect('/');
        }
    }
}
