<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProfileController extends Controller
{
    public function index($id)
    {
        $ambil = User::find($id);
        // dd($ambil);
        return view('home.content.profile',[
            'data' => $ambil
        ]);
    }
}
