<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = DB::table('mobils')
            ->join('data_mobils', 'mobils.id', '=', 'data_mobils.id')
            ->select('data_mobils.*', 'mobils.*')
            ->get();
        // dd($data);
        // dd($dataMobil);

        return view('home.content.rentcar', [
            'mobils' => $data,
        ]);
    }
}
