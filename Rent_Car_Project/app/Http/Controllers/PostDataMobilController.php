<?php

namespace App\Http\Controllers;

use App\Data_Mobil;
use App\Http\Controllers\Controller;
use App\Mobil;
use Illuminate\Http\Request;

class PostDataMobilController extends Controller
{
    public function index()
    {
        // dd(mobil::all());
        return view('admin.index', [
            'mobil' => mobil::all(),
        ]);
    }
    public function create()
    {
        return view('admin.create');
    }
    public function store(Request $request)
    {
        // dd($request->file('fotoMobil'));
        $request->file('fotoMobil')->move('images/fotoMobil');
        // $i = 0;

        mobil::create([

            'seri_mobil' => $request->seri,
            'harga_sewa' => $request->harga,
        ]);
        data_mobil::create([
            // 'mobil_id' => $i += 1,
            'nomor_mesin' => $request->nomorMesin,
            'tipe_mobil' => $request->tipe,
            'plat_nomor' => $request->platNomor,
            'tahun_mobil' => $request->tahun,
            'foto_mobil' => $request->file('fotoMobil'),
        ]);

        return redirect('/admin/mobil');
    }
    public function show($id)
    {
        $str = data_mobil::find($id)->foto_mobil;
        $name = mobil::find($id)->seri_mobil;
        $foto = explode("\\", $str);

        // dd($foto[3]);
        return view('admin.detail', [
            'name' => $name,
            'detail' => data_mobil::find($id),
            'fotoMobil' => $foto[3],
        ]);
        // tinggal diatur tampilan detailnya
    }
    public function edit($id)
    {
        $str = data_mobil::find($id)->foto_mobil;
        // dd($str);
        $foto = explode("\\", $str);
        // dd($foto[3]);
        return view('admin.edit', [
            'seri' => mobil::find($id),
            'detail' => data_mobil::find($id),
            'foto' => $foto[3],
        ]);
    }
    public function update($id, Request $request)
    {
        $request->file('fotoMobil')->move('images/fotoMobil');

        mobil::where('id', $id)
            ->update([
                'seri_mobil' => $request->seri,
                'harga_sewa' => $request->harga,
            ]);
        data_mobil::where('id', $id)
            ->update([
                'nomor_mesin' => $request->nomorMesin,
                'tipe_mobil' => $request->tipe,
                'plat_nomor' => $request->platNomor,
                'tahun_mobil' => $request->tahun,
                'foto_mobil' => $request->file('fotoMobil'),
            ]);
        return redirect('/admin/mobil');
    }
    public function destroy($id)
    {
        mobil::where('id', $id)->delete();
        data_mobil::where('id', $id)->delete();

        return redirect('/admin/mobil');
    }
}
