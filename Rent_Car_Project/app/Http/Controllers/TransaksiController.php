<?php

namespace App\Http\Controllers;

use App\mobil;
use App\transaksi;
use App\data_mobil;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;


class TransaksiController extends Controller
{
        public function store(Request $request){
            // dd($request->id);
            $acak = rand(99999999999, 1000000000000);
            // dd($acak);
            transaksi::create([
                'mobil_id' => $request->id_mobil,
                'user_id' => $request->id_user,
                'mulai_sewa' => $request->tgl_awal,
                'akhir_sewa' => $request->tgl_akhir,
                'jumlah_hari' => $request->lama_peminjam,
                'jenis_transaksi' => $request->jenis_transaksi,
                'tujuan' => $request->tujuan_peminjam,
                'status' => 0,
                'invoice' => $acak,
                'bukti' => 'null'
            ]);

            return redirect('/bukti_pembayaran/' . $request->id_mobil . '/'. $request->id_user);
        }

        public function bukti($id, $user)
        {
            $transaksi = transaksi::all()->where('user_id', $user);
            // dd($transaksi);
            $data = mobil::find($id);
            $details = data_mobil::find($id);
            return view('home.content.bukti_pembayaran',[
                'data_mobil' => $data,
                'detail' => $details,
                'pesanan' => $transaksi[$user-1]
            ]);
        }

        public function kirimBukti($id, Request $request){
            // dd($request->bukti);
            transaksi::where('id', $id)
            ->update([
                'bukti' => $request->bukti
            ]);

            return redirect('/homepage/dashboard');//pindah ke dashboard
        }
        public function dashboard()
        {
            $id_user = Auth::user()->id;
            $data_pesanan = DB::table('transaksis')->select(
                'mobil_id', 'user_id','mulai_sewa','akhir_sewa','jumlah_hari','jenis_transaksi','tujuan','status','invoice','bukti')
                ->where('user_id', $id_user)
                ->get();
            $pesanan = $data_pesanan->all();
            return view('home.content.dashboard-user',[
                'dataPesan' => $pesanan
            ]);
        }
        public function dashboardAdmin()
        {
            $data_pesanan = DB::table('transaksis')->select(
                'mobil_id', 'user_id','mulai_sewa','akhir_sewa','jumlah_hari','jenis_transaksi','tujuan','status','invoice','bukti')
                ->get();
            $pesanan = $data_pesanan->all();
            return view('adminUser.transaksi',[
                'dataPesan' => $pesanan
            ]);
        }
        public function pdf($id)
        {
            $user = Auth::user()->id;
            $transaksi = transaksi::all()->where('user_id', $user);
            // dd($transaksi);
            $data = mobil::find($id);
            $details = data_mobil::find($id);
            $datas = [
                'data_mobil' => $data,
                'detail' => $details,
                'pesanan' => $transaksi[$user-1]
            ];
            $test = compact('datas');
            // dd($test['datas']['pesanan']);
           
            // return view('home.content.nota', compact('datas'));

            $pdf = PDF::loadView('home.content.nota', compact('test'));
            return $pdf->download('invoice.pdf');
        }
}
