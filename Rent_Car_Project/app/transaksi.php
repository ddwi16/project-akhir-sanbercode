<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $fillable = ['mobil_id','user_id','mulai_sewa','akhir_sewa','jumlah_hari','jenis_transaksi','tujuan','status','invoice','bukti'];
}
