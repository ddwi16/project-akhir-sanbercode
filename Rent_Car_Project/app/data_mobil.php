<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_mobil extends Model
{
    protected $fillable = ['mobil_id','nomor_mesin','tipe_mobil','plat_nomor','tahun_mobil','foto_mobil'];

    public function mobil(){
        return $this->belongsTo('App\mobil');
      }
}
