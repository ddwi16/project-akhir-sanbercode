<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_ktp extends Model
{
    protected $fillable = ['nama_lengkap','nomor_ktp','foto_ktp','gender'];
}
