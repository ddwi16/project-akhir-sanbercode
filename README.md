# Project Akhir Sanbercode

# Rent Car Project
## Kelompok 15
Anggota Kelompok :
1. Rio Aditya Novanto
2. Rifqy Fachrizi
3. Dicky Dwi Darmawan

Tema : Rental Sewa Mobil

## ERD
![ERD](/erd-rentcar.png)

## Link Video
https://gitlab.com/ddwi16/project-akhir-sanbercode/-/blob/main/video%20demo%20rent%20car.mp4?expanded=true&viewer=rich

## Link Hosting
https://heroku-rent-car-project.herokuapp.com/
